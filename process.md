## Prepare your metadata
- This helps: https://pool.pm/test/metadata
- When your metadata is ready, save it as a `.json` file - we'll need it

## Get Address
- I like to save mine as the environment variable `SENDER` for easy access

## Get UTxO
`cardano-cli query utxo --mainnet --address $SENDER`

## Set SENDERKEY
I like to set the path to my `payment.skey` as the environment variable `SENDERKEY`

## Get protocol parameters
`cardano-cli query protocol-parameters --mainnet --out-file protocol.json`

## Create Policy
```
mkdir policy

cardano-cli address key-gen \
--verification-key-file policy/policy.vkey \
--signing-key-file policy/policy.skey

touch policy/script.policy
cardano-cli address key-hash --payment-verification-key-file policy/policy.vkey

```

Then put that new hash into `policy.script`, which should now look like this:

```
{
    "type": "all",
    "scripts": [
        {
            "keyHash": "YOUR_KEYHASH_HERE",
            "type": "sig"
        },
        {
            "slot": DO THE ARITHMETIC and PUT A SLOT NUMBER HERE
            "type": "before
        }
    ]
}
```

## Mint the new asset:
```
cardano-cli transaction policyid --script-file ./policy/policy.script
```

Now you have a new Policy ID!

Place it in the metadata for your new asset.

## Create the raw transaction
```
cardano-cli transaction build-raw \
--tx-in $TXIN \
--tx-out $SENDER+$LOVELACE+"1 4766a6108cb1abf8a2c13f4c1ed49810b334f4a20721198e52f2b68a.csk008unsig" \
--mint="1 4766a6108cb1abf8a2c13f4c1ed49810b334f4a20721198e52f2b68a.csk008unsig" \
--mint-script-file policy/policy.script \
--metadata-json-file 914002.json \
--fee 0 \
--out-file tx.draft
```

## Calculate Fee
```
cardano-cli transaction calculate-min-fee \
--tx-body-file tx.draft \
--tx-in-count 1 \
--tx-out-count 1 \
--witness-count 2 \
--protocol-params-file protocol.json
```

## Arithmetic:
Yeah, this needs to be one line:
```
expr $LOVELACE - $FEE
CHANGE=
```

## Draft Real TX
```
cardano-cli transaction build-raw \
--tx-in $TXIN \
--tx-out $SENDER+$CHANGE+"1 4766a6108cb1abf8a2c13f4c1ed49810b334f4a20721198e52f2b68a.csk008unsig" \
--mint="1 4766a6108cb1abf8a2c13f4c1ed49810b334f4a20721198e52f2b68a.csk008unsig" \
--mint-script-file policy/policy.script \
--metadata-json-file 914002.json \
--fee $FEE \
--out-file tx.raw
```

## Sign TX
```
cardano-cli transaction sign \
--signing-key-file $SENDERKEY \
--signing-key-file policy/policy.skey \
--tx-body-file tx.raw \
--out-file tx.signed
```

## Submit TX
```
cardano-cli transaction submit \
--tx-file tx.signed \
--mainnet
```
