unsig = {'index': 914001,
         'num_props': 2,
         'properties': {
             'multipliers'   : [40,20],
             'colors'        : ['Red','Red'],
             'distributions' : ['CDF','CDF'],
             'rotations'     : [90,180]}}

unsig = {'index': 914002,
         'num_props': 5,
         'properties': {
             'multipliers'   : [20,10,5,2.5,1.25],
             'colors'        : ['Blue', 'Blue', 'Blue', 'Blue','Blue'],
             'distributions' : ['Normal', 'Normal', 'Normal', 'Normal','Normal'],
             'rotations'     : [0, 90, 180, 270, 360]}}

unsig = {'index': 914003,
         'num_props': 6,
         'properties': {
             'multipliers'   : [40,20,10,5,2.5,1.25],
             'colors'        : ['Green', 'Green', 'Green', 'Green','Green','Green'],
             'distributions' : ['Normal', 'Normal', 'Normal', 'Normal','Normal','Normal'],
             'rotations'     : [0, 90, 180, 270, 360, 90]}}

unsig = {'index': 914004,
         'num_props': 3,
         'properties': {
             'multipliers'   : [3,0.3,1.65],
             'colors'        : ['Red','Green','Blue'],
             'distributions' : ['CDF','Normal','CDF'],
             'rotations'     : [135,90,225]}}

unsig = {'index': 914005,
         'num_props': 5,
         'properties': {
             'multipliers'   : [0.9,0.9,1.8,1.8,2.7],
             'colors'        : ['Green', 'Blue', 'Blue', 'Red', 'Green'],
             'distributions' : ['CDF', 'CDF', 'CDF', 'CDF', 'CDF'],
             'rotations'     : [0, 180, 0, 180, 0]}}

unsig = {'index': 914006,
         'num_props': 5,
         'properties': {
             'multipliers'   : [6,3,1.5,0.75,0.375],
             'colors'        : ['Blue', 'Red', 'Blue', 'Green','Red'],
             'distributions' : ['Normal', 'Normal', 'Normal', 'Normal','Normal'],
             'rotations'     : [0, 0, 180, 0, 180]}}

unsig = {'index': 914007,
         'num_props': 4,
         'properties': {
             'multipliers'   : [3,0.3,1.65,6],
             'colors'        : ['Red','Green','Blue','Green'],
             'distributions' : ['CDF','Normal','CDF','Normal'],
             'rotations'     : [135,90,225,45]}}

unsig = {'index': 914008,
         'num_props': 4,
         'properties': {
             'multipliers'   : [3,0.3,1.65,6],
             'colors'        : ['Red','Green','Blue','Green'],
             'distributions' : ['CDF','CDF','CDF','CDF'],
             'rotations'     : [135,90,225,45]}}

unsig = {'index': 914009,
         'num_props': 4,
         'properties': {
             'multipliers'   : [2.2,2.2,2.2,2.2],
             'colors'        : ['Green', 'Blue', 'Red', 'Blue'],
             'distributions' : ['CDF', 'CDF', 'CDF', 'CDF'],
             'rotations'     : [0, 90, 180, 270]}}

unsig = {'index': 914010,
         'num_props': 5,
         'properties': {
             'multipliers'   : [3.3,3.3,3.3,3.3,3.3],
             'colors'        : ['Green', 'Blue', 'Red', 'Blue','Green'],
             'distributions' : ['CDF', 'CDF', 'CDF', 'CDF','CDF'],
             'rotations'     : [0, 180, 0, 180, 0]}}

unsig = {'index': 914011,
         'num_props': 6,
         'properties': {
             'multipliers'   : [2.5,2.5,2.5,2.5,2,2],
             'colors'        : ['Green', 'Green', 'Red', 'Red', 'Blue', 'Blue'],
             'distributions' : ['CDF', 'CDF', 'CDF', 'CDF', 'CDF', 'CDF'],
             'rotations'     : [0, 90, 0, 90, 0, 90]}}

unsig = {'index': 914012,
         'num_props': 6,
         'properties': {
             'multipliers'   : [2.5,2.5,2.5,2.5,2,2],
             'colors'        : ['Green', 'Green', 'Red', 'Red', 'Blue', 'Blue'],
             'distributions' : ['CDF', 'CDF', 'CDF', 'CDF', 'CDF', 'CDF'],
             'rotations'     : [270, 180, 270, 180, 270, 180]}}

unsig = {'index': 914013,
         'num_props': 6,
         'properties': {
             'multipliers'   : [1.5,1.5,1.5,1.5,2,2],
             'colors'        : ['Blue', 'Blue', 'Green', 'Green', 'Red', 'Red'],
             'distributions' : ['CDF', 'CDF', 'CDF', 'CDF', 'CDF', 'CDF'],
             'rotations'     : [0, 90, 0, 90, 0, 90]}}

unsig = {'index': 914014,
         'num_props': 6,
         'properties': {
             'multipliers'   : [1.5,1.5,5,5,5,5],
             'colors'        : ['Red', 'Red', 'Blue', 'Blue', 'Green', 'Green'],
             'distributions' : ['CDF', 'CDF', 'CDF', 'CDF', 'CDF', 'CDF'],
             'rotations'     : [0, 90, 0, 90, 0, 90]}}

unsig = {'index': 914015,
         'num_props': 6,
         'properties': {
             'multipliers'   : [10,10,10,10,2,2],
             'colors'        : ['Red', 'Red', 'Blue', 'Blue', 'Green', 'Green'],
             'distributions' : ['CDF', 'CDF', 'CDF', 'CDF', 'CDF', 'CDF'],
             'rotations'     : [0, 90, 0, 90, 0, 90]}}

unsig = {'index': 917016,
         'num_props': 6,
         'properties': {
             'multipliers'   : [0.8,0.8,0.8,0.8,1.6,1.6],
             'colors'        : ['Green', 'Blue', 'Red', 'Red', 'Blue', 'Blue'],
             'distributions' : ['CDF', 'CDF', 'CDF', 'CDF', 'CDF', 'CDF'],
             'rotations'     : [270, 180, 270, 180, 270, 180]}}

unsig = {'index': 917017,
         'num_props': 5,
         'properties': {
             'multipliers'   : [0.8,0.8,0.8,0.8,1.6],
             'colors'        : ['Green', 'Blue', 'Red', 'Red', 'Blue'],
             'distributions' : ['CDF', 'CDF', 'CDF', 'CDF', 'CDF'],
             'rotations'     : [270, 180, 270, 180, 270]}}


